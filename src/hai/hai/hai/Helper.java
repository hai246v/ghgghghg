package hai.hai.hai;

public final class Helper {
    public static boolean isPrimeNumber(int n) {
        if (n == 0 || n == 1) return false;
        for (int i = 2; i < n; i++) {
            if (n%i == 0) return false;
        }
        return true;
    }

    public static Tile createTileFromChar(char c) {
        switch (c) {
            case '@':
                return new Tile.Empty();
            case '/':
                return new Tile.Wall();
            case 'H':
                return new Tile.HolyWater();
            case 'u':
                return new Tile.PowerUp(300);
            case 'U':
                return new Tile.PowerUpWithShield(999);
            case 'c':
                return new Tile.GoldChest(500);
            case 'C':
                return new Tile.GoldChest(1000);
            case 'B':
                return new Tile.Bandit();
            case 'M':
                return new Tile.Mummy();
            case 'i':
                return new Tile.InvisibleSpell();
            case 't':
                return new Tile.PoisonTrap();
            case 'T':
                return new Tile.FireballTrap();
        }
        return new Tile.Empty();
    }
}
