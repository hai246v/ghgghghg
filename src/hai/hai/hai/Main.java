package hai.hai.hai;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {
        try {
            Problem problem = new Problem(new String(Files.readAllBytes(Paths.get("input.txt"))));
            problem.solve();
            BufferedWriter out = new BufferedWriter(new FileWriter("output.txt"));
            out.write(problem.getOutput());
            out.flush();

        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
