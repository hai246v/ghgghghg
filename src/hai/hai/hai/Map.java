package hai.hai.hai;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class Map {
    private ArrayList<ArrayList<Tile>> tiles;

    public Map() {
        tiles = new ArrayList<>();
        IntStream.range(0, 12).forEach(i -> {
            ArrayList<Tile> row = new ArrayList<>();
            IntStream.range(0, 16).forEach(j -> row.add(new Tile.Empty()));
            tiles.add(row);
        });
    }

    public Tile getTileAt(int x,int y) {
        return tiles.get(y).get(x);
    }

    public void setTileAt(int x,int y, Tile tile) {
        tiles.get(y).set(x, tile);
    }

    public void destroyTile(int x, int y) {
        tiles.get(y).set(x, new Tile.Empty());
    }
}
