package hai.hai.hai;

abstract class Tile {
    protected boolean markedToBeDestroyed = false;

    protected boolean isMummified () {
        return false;
    }

    public void onCharacterEnter(Character character) {
        //System.out.println(this.toString());
    }

    public boolean isSolid () {
        return false;
    }

    public boolean isMarkedToBeDestroyed() {
        return markedToBeDestroyed;
    }

    public static class Empty extends Tile {

    }

    public static class Wall extends Tile {
        @Override
        public boolean isSolid() {
            return true;
        }
    }


    public static class GoldChest extends Tile {
        protected int goldReward;

        public GoldChest(int goldReward) {
            this.goldReward = goldReward;
        }

        @Override
        public void onCharacterEnter(Character character) {
            super.onCharacterEnter(character);
            if (!character.isMummified()) {
                character.addGold(goldReward);
                markedToBeDestroyed = true;
            }
        }
    }

    public static class Mimic extends GoldChest {
        protected int damage;

        public Mimic(int damage, int goldReward) {
            super(goldReward);
            this.damage = damage;
        }

        @Override
        public void onCharacterEnter(Character character) {
            if (character.isBlessedByFriends()) markedToBeDestroyed = true;
            else if (character.isPrayedFor()) super.onCharacterEnter(character);
            else character.reduceHealth(damage);

        }
    }

    public static class PowerUp extends Tile {
        protected int healAmount;

        public PowerUp(int healAmount) {
            this.healAmount = healAmount;
        }

        @Override
        public void onCharacterEnter(Character character) {
            super.onCharacterEnter(character);
            if (!character.isMummified()) {
                character.addHealth(healAmount);
                markedToBeDestroyed = true;
            }
        }
    }

    public static class PowerUpWithShield extends PowerUp {
        public PowerUpWithShield(int healAmount) {
            super(healAmount);
        }

        @Override
        public void onCharacterEnter(Character character) {
            if (!character.isMummified()) character.addShield();
            super.onCharacterEnter(character);
        }
    }

    public static class FireballTrap extends Tile {
        @Override
        public void onCharacterEnter(Character character) {
            super.onCharacterEnter(character);
            character.reduceHealth(400);
        }
    }

    public static class PoisonTrap extends Tile {
        @Override
        public void onCharacterEnter(Character character) {
            super.onCharacterEnter(character);
            character.poison(3);
        }
    }

    public static class Mummy extends Tile {
        @Override
        public void onCharacterEnter(Character character) {
            super.onCharacterEnter(character);
            if (character.isInvisible()) return;
            if (!character.isBlessedByTA()) {
                character.mummify();
                character.reduceHealth(200);
            } else {
                markedToBeDestroyed = true;
            }
        }
    }

    public static class Bandit extends Tile {
        private boolean mummified = false;

        @Override
        public void onCharacterEnter(Character character) {
            super.onCharacterEnter(character);
            if (character.isInvisible()) return;
            if (character.isMummified()) mummified = true;
            if (character.isHealthSquare()) {
                markedToBeDestroyed = true;
            } else if (character.getGold() > 2000) {
                character.reduceGold(2000);
            } else {
                character.reduceHealth((int) Math.floor((
                        1.0 - (double) character.getGold() / 2000) * 500));
                character.reduceGold(9999);
            }
        }

        @Override
        public boolean isMummified() {
            return mummified;
        }
    }

    public static class HolyWater extends Tile {
        @Override
        public void onCharacterEnter(Character character) {
            super.onCharacterEnter(character);
            character.cureMummified();
            character.curePoison();
        }
    }

    public static class InvisibleSpell extends Tile {
        @Override
        public void onCharacterEnter(Character character) {
            super.onCharacterEnter(character);
            if (!character.isMummified()) character.beInvisible(5);
        }
    }
}
