package hai.hai.hai;

public enum Direction {
    UP, DOWN, LEFT, RIGHT;

    public static Direction fromChar(char c) {
        switch (c) {
            case '^':
                return UP;
            case 'v':
                return DOWN;
            case '<':
                return LEFT;
            case '>':
                return RIGHT;
        }
        return UP;
    }

    public int getMovementX() {
        switch (this) {
            case LEFT:
                return -1;
            case RIGHT:
                return 1;
        }
        // Other cases
        return 0;
    }

    public int getMovementY() {
        switch (this) {
            case UP:
                return -1;
            case DOWN:
                return 1;
        }
        // Other cases
        return 0;
    }
}
