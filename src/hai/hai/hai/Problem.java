package hai.hai.hai;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class Problem {
    private String input;
    private String output;
    private ArrayList<Direction> moves;
    private Map map;
    private Character character;

    public void solve() {
        parseInput();
        for (Direction move : moves) {
            int targetX = character.getX() + move.getMovementX();
            int targetY = character.getY() + move.getMovementY();
            if (!map.getTileAt(targetX, targetY).isSolid()) {
                character.setX(targetX);
                character.setY(targetY);
                map.getTileAt(targetX,targetY).onCharacterEnter(character);
                if (map.getTileAt(targetX, targetY).isMarkedToBeDestroyed())
                    map.setTileAt(targetX, targetY, new Tile.Empty());
                if (map.getTileAt(targetX, targetY).isMummified())
                    map.setTileAt(targetX, targetY, new Tile.Mummy());

                appendLineToOutput("Gold: " + character.getGold() + " Health: " + character.getHealth());
                if (character.getGold() > 5000) {
                    appendLineToOutput("WIN");
                    return;
                }
                if (character.isDead()) {
                    appendLineToOutput("LOSE");
                    return;
                }
            }
        }
        appendLineToOutput("LOSE");
    }

    public Problem(String input) {
        this.input = "";
        this.input = input;
        moves = new ArrayList<>();
        map = new Map();
        output = "";
    }

    public String getOutput() {
        return output;
    }

    private void parseInput() {
        String lines[] = input.split("\n");
        parseMap(lines);
        parseCharacter(lines);
        parseMoves(lines);
    }

    private void parseMap(String[] lines) {
        IntStream.range(0, 12).forEach(i -> IntStream.range(0, 16).forEach(i1 ->
                map.setTileAt(i1, i, Helper.createTileFromChar(lines[i].charAt(i1)))));
    }

    private void parseMoves(String[] lines) {
        for (int i = 13; i < lines.length; i++) {
            moves.add(Direction.fromChar(lines[i].charAt(0)));
        }
    }

    private void parseCharacter(String[] lines) {
        AtomicInteger x = new AtomicInteger();
        AtomicInteger y = new AtomicInteger();
        IntStream.range(0, 12).forEach(i -> IntStream.range(0, 16).forEach(i1 -> {
            if (lines[i].charAt(i1) == '@') {
                x.set(i1);
                y.set(i);
            }
        }));
        character = new Character(
                Integer.parseInt(lines[12].split(" ")[1]),
                Integer.parseInt(lines[12].split(" ")[0]),
                x.get(), y.get()
        );
    }

    private void appendLineToOutput(String s) {
        output = output.concat(s+"\n");
    }
}

