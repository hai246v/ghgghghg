package hai.hai.hai;

public class Character {
    private int gold;
    private int health;
    private int x;
    private int y;
    private int poisoned = 0;
    private boolean mummified = false;
    private int invisible = 0;
    private boolean shield = false;

    public Character(int initialGold, int initialHealth, int initialX, int initialY) {
        gold = initialGold;
        health = initialHealth;
        x = initialX;
        y = initialY;
    }

    public void onMove() {
        if (poisoned > 0) health -= 100;
        if (invisible > 0) invisible--;
        if (poisoned > 0) poisoned--;
    }

    public int getHealth() {
        return health;
    }

    public void reduceHealth(int amount) {
        if (hasShield()) shield = false;
        else health -= amount;
        if (health < 0) health = 0;
    }

    public void addHealth(int amount) {
        health += amount;
        // Health can not be > 999
        if (health > 999) health = 999;
    }

    public boolean isHealthSquare() {
        return (Math.sqrt((double) health)) == (int) (Math.sqrt((double) health));
    }

    public void reduceGold(int amount) {
        gold -= amount;
        if (gold < 0) gold = 0;
    }

    public void addGold(int amount) {
        gold += amount;
    }

    public int getGold() {
        return gold;
    }


    public void beInvisible(int turns) {
        this.invisible = turns;
    }

    public void poison(int turns) {
        poisoned += turns;
    }

    public void mummify() {
        mummified = true;
    }

    public boolean isMummified() {
        return mummified;
    }

    public boolean isPoisoned() {
        return poisoned > 0;
    }

    public boolean isBlessedByFriends() {
        return Helper.isPrimeNumber(health);
    }

    public boolean isBlessedByTA() {
        return health == 666;
    }

    public boolean isPrayedFor() {
        return health == 777;
    }

    public boolean isDead() {
        return health <= 0;
    }


    public boolean isInvisible() {
        return invisible > 0;
    }

    public boolean hasShield() {
        return shield;
    }

    public void addShield() {
        this.shield = true;
    }

    public void cureMummified() {
        mummified = false;
    }

    public void curePoison() {
        poisoned = 0;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
